# BT Relay - Firmware

This repository contains the code for the microcontroller. It is programmed
using MPLAB 8 & XC8 compiler from Microchip.

The firmware features a simple serial protocol used to control the
relay, get its status, turn it on/off after N seconds, etc.